library('dplyr')

PCA_normalize <- function (df, yname) {
  # Standardize the df variables
  # @param df data frame
  # @return matrix with elements normalized to mean and standard deviation
  dfx <- df %>% select(-yname)
  Z <- matrix(0.0, nrow=nrow(dfx), ncol=ncol(dfx))
  for(col in 1:ncol(dfx)) {
  	meanVal <- mean(dfx[,col])
    stdVal <- sd(dfx[,col])
  	for(row in 1:nrow(dfx)) {
  		Z[row, col] <- (dfx[row, col] - meanVal) / stdVal
  	}
  }
  return (Z)
}

PCA_build <- function (df, yname) {
  # Compute the eigenvalues and and eigenvectors of the correlation matrix
  # @param df data frame
  # @param yname name of the column acting as dependent variable
  # @return PCA object

  # standardize the dfx variables
  Z <- PCA_normalize(df, yname)
  # build the correlation matrix
  C <- cov(Z)
  # compute the eigenvectors and eigenvalues
  eig <- eigen(C)
  C_eigenval<- eig$values
  C_eigenvec <- eig$vectors

  return(list("eigenvalues" = C_eigenval, "eigenvectors" = C_eigenvec, "Z" = Z, "yname" = yname, "df" = df))
}

#Write A function to help choose the number of PC to keep
#and then create a new df using pc

PCA_choose <- function (pca_obj, numPC, df, yname) {
  # Choose the number of principal components to keep and
  # return a new data frame with the reduced, transformed 
  # variables and the output variable
  #
  # @param pca_obj return value of PCA_build
  # @param numPC number of principal components to keep
  # @param df data frame with columns transformed by the principal component analysis
  # @param yname response variable in df
  # @return new, reduced data frame with transformed input

  nc <- ncol(df)

  # make sure numPC is number of columns or smaller
  numPC <- max(1, min(nc - 1, numPC))

  #create a new df with the transformed variables
  nr <- nrow(df)

  #n=numPC number of vectors

  Z <- PCA_normalize(df, yname)
  mat_out <- matrix(0.0, nrow=nr, ncol=numPC)
  for (ipc in 1:numPC) {

    for (row in 1:nr) {
    	mat_out[row, ipc] <- pca_obj$eigenvectors[, ipc] %*% Z[row, ]
    }

  }

  # turn the matrix into a data frame
  df_out <- as.data.frame(mat_out)

  # add the y column
  df_out[,yname] <- df[,pca_obj$yname]

  return(df_out)  
}

PCA_predict <- function (df_train, yname, numComp, df_test){
  #@param df_train training raw df
  #@param yname response variable
  #@param numComp number of chosen principal components
  #@param df_test testing set of the raw df
  #@return the predictions for df_test
  pca_obj <- PCA_build(df_train, yname)
  #build the linear model formula
  formula <- paste(yname, " ~ ")
  for (i in 1:numComp){
    vNum <- paste("V", i, sep = "")
    formula <- paste(formula, vNum)
    #Add another + if still more predictors
    if (i < numComp){
      formula <- paste(formula , " + ")
    }
  }
  df_pca_train <- PCA_choose(pca_obj, numComp, df_train, yname)
  model <- lm (as.formula(formula), data = df_pca_train)
  df_pca_test <- PCA_choose (pca_obj, numComp, df_test, yname)
  res <- predict(model, newdata = df_pca_test)
  return(res)

}


