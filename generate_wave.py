import argparse
import numpy
import pandas

parser = argparse.ArgumentParser(description='Generate 1d data')
parser.add_argument('-p', type=int, default=5, 
                    help='number of x points (number of columns)')
parser.add_argument('-t', type=int, default=10, 
                    help='number of t points (number of rows)')
parser.add_argument('-v', type=float, default=1.234567, 
                    help='velocity')
parser.add_argument('-s', type=int, default=123, 
                    help='random seed')
parser.add_argument('-n', type=float, default=0.10, 
	                help='noise amplitude')
parser.add_argument('-o', type=str, default='wave.csv',
	                help='output CSV file')

args = parser.parse_args()

numpy.random.seed(args.s)
xs = numpy.random.rand(args.p)
ts = numpy.linspace(0., 1., args.t)

xx, tt = numpy.meshgrid(xs, ts, indexing='xy')
yy = xx - args.v * tt
noise = 2*args.n * (numpy.random.rand(xx.shape[0], xx.shape[1]) - 0.5)
zz = numpy.sin(4*numpy.pi*yy) + noise

df = pandas.DataFrame(zz, columns=['x{}'.format(x) for x in range(args.p)])
df.describe()
print('writing data in CSV file {}'.format(args.o))
df.to_csv(args.o, sep=',', index=False)

